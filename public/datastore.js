﻿
(function () {

    var data = {
        columns: [
            { name: 'id', sortOrder: null, sortDirection: null, type: 'detail' },
            { name: 'value', sortOrder: null, sortDirection: null, type: 'detail' },
            { name: 'active', sortOrder: null, sortDirection: null, type: 'detail' },
            { name: 'g1', sortOrder: null, sortDiretion: null, type: 'group' },
            { name: 'g2', sortOrder: null, sortDiretion: null, type: 'group' }
        ],        
        dataConverted:
        {
            id: 0,
            type: 'group',
            isCollapsed: false,
            header: { id: 1, value: 'nested-list', key: 'vue' },
            items:
                [
                    {
                        id: 'z1',
                        type: 'group',
                        header: { id: 1, value: '2000', key: 'Year' },
                        depth: 0,
                        isCollapsed: false,
                        items:
                            [
                                {
                                    id: 'z2',
                                    type: 'detail',
                                    depth: 1,
                                    header: { id: 10, value: 'Jan', key: 'Month' },
                                    isCollapsed: false,
                                    pagination: {
                                        pageNumber: 1,
                                        countPerPage: 10,
                                        total: 20
                                    },
                                    columns: [
                                        { name: 'id', dataType: 'int', sort: { order: null, direction: null }, uuid: null },
                                        { name: 'value', dataType:'int', sort: { order: null, direction: null }, uuid: null },
                                        { name: 'active', dataType:'bool', sort: { order: null, direction: null }, uuid: null },
                                        { name: 'category', dataType: 'string', sort: { order: null, direction: null }, uuid: null },
                                    ],
                                    details: [
                                        { id: 1, value: 200, active: false, category: 'a', },
                                        { id: 2, value: 190, active: false, category: 'b', },
                                        { id: 3, value: 180, active: false, category: 'c', },
                                        { id: 4, value: 160, active: true, category: 'c', },
                                        { id: 5, value: 160, active: false, category: 'c', },
                                        { id: 6, value: 160, active: true, category: 'd', },
                                        { id: 7, value: 140, active: false, category: 'a', },
                                        { id: 8, value: 160, active: false, category: 'b', },
                                        { id: 9, value: 120, active: false, category: 'c', },
                                        { id: 10, value: 110, active: false, category: 'a', },
                                        { id: 11, value: 100, active: false, category: 'b', },
                                        { id: 12, value: 900, active: false, category: 'c', },
                                        { id: 13, value: 800, active: false, category: 'a', },
                                        { id: 14, value: 70, active: true, category: 'b', },
                                        { id: 15, value: 60, active: true, category: 'c', },
                                        { id: 16, value: 500, active: false, category: 'b', },
                                        { id: 17, value: 500, active: false, category: 'b', },
                                        { id: 18, value: 500, active: true, category: 'b', },
                                        { id: 19, value: 500, active: false, category: 'a', },
                                        { id: 20, value: 10, active: false, category: 'a', }
                                    ]
                                },
                                {
                                    id: 'z3',
                                    type: 'detail',
                                    header: { id: 20, value: 'Feb', key: 'Month' },
                                    isCollapsed: false,
                                    depth: 1,
                                    pagination: {
                                        pageNumber: 1,
                                        countPerPage: 10,
                                        total: 2
                                    },
                                    columns: [
                                        { name: 'id', dataType: 'int', sort: { order: null, direction: null }, uuid: null },
                                        { name: 'value', dataType: 'int', sort: { order: null, direction: null }, uuid: null },
                                        { name: 'active', dataType: 'bool', sort: { order: null, direction: null }, uuid: null },
                                        { name: 'category', dataType: 'string', sort: { order: null, direction: null }, uuid: null },
                                    ],
                                    details: [
                                        { id: 3, value: 70, active: false, category: 'a' },
                                        { id: 4, value: 80, active: false, category: 'z', }
                                    ]
                                }
                            ]

                    },
                ]
        },        
    };

    var getData = function () {
        return JSON.parse(JSON.stringify(data.dataConverted));
    }

    if (!window.gridDataStore) {
        window.gridDataStore = {getData: getData};
    }    
})();