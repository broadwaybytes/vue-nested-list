﻿(function(){
    window.vueApp = Vue.createApp({
        el: "#vueApp",
        data: function () {
            return { pageName: 'grid vue demo' }
        },
        components: {
            GridVueWrapper: Vue.defineAsyncComponent(function () {
                return window.vue_component_lib.GridVueWrapper();
            })            
        },
        created: function () {
            console.log("vue app created");
        },
    });

    vueApp.mount('#vueApp');
})();

