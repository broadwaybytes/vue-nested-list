﻿
(function () {
        
    var getModel = function (template) {
        return {
            name: 'grid-vue',
            inheritAttrs: false,
            template: template,            
            props: {
                gridData: Object,
            },
            data: function () {
                var data = {
                    isCollapsed: false
                };
                return data;
            },
            components: {
                GridVueDetails: Vue.defineAsyncComponent(function () {
                    return window.vue_component_lib.GridVueDetails();
                }),
            },            
            computed: {
            },
            created: function () {
                console.log('grid created');
            },
            methods: {
                onSliderClick: function () {
                    this.isCollapsed = !this.isCollapsed;
                },
                getSliderText: function (item) {
                    return item.isCollapsed ? '+' : '-';
                },
                getObjectKeys: function (obj) {
                    var keys = [];
                    for (var p in obj) {
                        if (!obj.hasOwnProperty(p)) {
                            break;
                        }
                        keys.push(p);
                    }
                    return _.chain(keys)
                        .sortBy(function (a) {
                            return a;
                        }).value();
                }
            },
            mounted: function () {
                console.log(this.name + ' mounted');
            }
        };
    }

    window.vue_component_lib.GridVue = function () {
        return jQueryLoadHtml("./grid/grid-vue.html")
            .then(function (resp) {
                var model = getModel(resp);
                return model;
            }).catch(function (err) {
                console.log(err.message);
            });        
    }
    
})();
