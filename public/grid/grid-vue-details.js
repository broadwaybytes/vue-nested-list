﻿(function () {
    //-------------------------------------------------------------------------
    //actual sorting
    //-------------------------------------------------------------------------
    function sortByDataType(a, b, direction, name, dataType) {
        if (a == null || b == null) {
            return 0;
        }

        var dir = 0;
        if (direction == 'asc'){
            dir = 1;
        }else if (direction == 'dsc'){
            dir = -1;
        }

        if (dataType == 'string') {
            if (a[name] < b[name])
                return -1 * dir;
            if (a[name] > b[name])
                return 1 * dir;
            return 0;
        }

        if (dataType == 'date') {
            a = Date.parse(a[name]);
            b = Date.parse(b[name]);
        }else if (dataType == 'int') {
            a = parseInt(a[name]);
            b = parseInt(b[name]);
        }else if (dataType == 'float') {
            a = parseFloat(a[name]);
            b = parseFloat(b[name]);
        }else if (dataType == 'bool'){  
            a = a[name] == true?0:1
            b = b[name] == true?0:1
        }

        return (a - b) * dir ;
    }

    function sortAcutally(self) {
        var arr = self.$data.columns.filter(function (a) {
            return a.sort.order;
        })

        if (arr == null || arr.length === 0) {
            return;
        }

        arr = arr.sort(function (a, b) {
            return a.sort.order - b.sort.order;
        });

        var sortFuncs = [];

        arr.forEach(function(x){
            var func = function(a,b) {
                return sortByDataType(a,b, x.sort.direction, x.name, x.dataType)
            }
            sortFuncs.push(func);
        });
        
        self.$data.details = self.$data.details
            .sort(firstBy(sortFuncs[0]).thenBy(sortFuncs[1])
            .thenBy(sortFuncs[2]).thenBy(sortFuncs[3]));
    }

    //-----------------------------------------------------------------
    //sort order update 1,2,3,4 etc
    //-----------------------------------------------------------------
    function setSortOrder(column, self) {
        if (column.sort.direction !== null) {
            setSortOrder_NotClear(column, self);
            return;
        }

        setSortOrder_Clear(column, self);
    }

    function setSortOrder_reset(self) {
        var sortedColumns = self.$data.columns.filter(function (a) {
            return a.sort.direction != null;
        });

        if (sortedColumns == null || sortedColumns.length === 0) {
            return;
        }

        var sortedColumns = sortedColumns.sort(function (a, b) {
            var a = a.sort.order;
            var b = b.sort.order;
            return a - b;
        });

        var columnHash = {};
        var i = 1;
        sortedColumns.forEach(function (h) {
            columnHash[h.name] = i;
            i++;
        });

        self.$data.columns.forEach(function (i) {
            var sortOrder = columnHash[i.name];
            if (!sortOrder) {
                return;
            }
            i.sort.order = sortOrder;
        });
    }

    function setSortOrder_Clear(column, self) {
        column.sort.order = null;
        column.sort.direction = null;

        setSortOrder_reset(self);
    }

    function setSortOrder_NotClear(header, self) {
        var maxOrderColumn = _.chain(self.$data.columns).maxBy
            (function (i) {
                return i.sort.order;
            }).value();

        if (maxOrderColumn == null) {
            header.sort.order = 1;
            return;
        }

        //the header has max order
        if (header.sort.order == maxOrderColumn.sort.order) {
            return;
        }

        //increment order
        header.sort.order = maxOrderColumn.sort.order + 1;

        setSortOrder_reset(self);
    }

    //---------------------------------------------------------
    //map data
    //---------------------------------------------------------
    var mapData = function(propsDetail){
        var data = JSON.parse(JSON.stringify(propsDetail));
        return data;              
    }

    //---------------------------------------------------------
    //actual model
    //---------------------------------------------------------
    var getModel = function (template) {
        return {
            name: 'grid-vue-details',
            template: template,
            inheritAttrs: false,
            props: {
                detail: Object,
            },
            data: function () {
                return mapData(this.$props.detail);
            },
            computed: {
                numberOfPages: function () {
                    var pag = this.$data.pagination;
                    return Math.ceil(pag.total / pag.countPerPage);
                }
            },
            created: function () {
                console.log('grid view details created');
            },
            methods: {
                onSliderClick: function () {
                    this.isCollapsed = !this.isCollapsed;
                },
                onSortClick: function(column){
                    if (column.sort.direction==="asc"){
                        column.sort.direction = "dsc";
                    }else if (column.sort.direction==="dsc"){
                        column.sort.direction = null;
                    }else{
                        column.sort.direction = "asc";
                    }
                    setSortOrder(column, this);
                    sortAcutally(this);
                },
                getPaginatedData: function (obj) {
                    var pag = obj.pagination;
                    var x = _.chain(obj.details).drop(pag.countPerPage * (pag.pageNumber - 1))
                        .take(pag.countPerPage).value();
                    return x;
                },
                pagePrevious: function (obj) {
                    var pag = obj.pagination;
                    if (pag.pageNumber == 1) {
                        return;
                    }
                    pag.pageNumber--;
                },
                pageNext: function (obj) {
                    var pag = obj.pagination;
                    if (Math.ceil(pag.total / pag.countPerPage) == pag.pageNumber) {
                        return;
                    }
                    pag.pageNumber++;
                },
                isLastPage: function () {
                    var pag = this.$data.pagination;
                    return this.numberOfPages == pag.pageNumber;
                },
                
            },
            mounted: function () {
                console.log(this.name + ' mounted');
            }
        };
    }

    window.vue_component_lib.GridVueDetails = function () {
        return jQueryLoadHtml('./grid/grid-vue-details.html')
            .then(function (resp) {
                var model = getModel(resp);
                return model;
            }).catch(function (err) {
                console.log(err.message);
            });
    }    
})();
