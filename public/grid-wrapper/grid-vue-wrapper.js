﻿(function () {
    var getModel = function (template) {
        return {
            name: 'grid-vue-wrapper',
            template: template,
            data: function () {
                return {
                    gridData: window.gridDataStore.getData()
                };
            },
            components: {
                GridVue: Vue.defineAsyncComponent(function () {
                    return window.vue_component_lib.GridVue();
                }),
            },
            created: function () {
                console.log('grid view wrapperd created');
                window.gridWrapper = this;
            },
            methods: {
            }
        };
    }

    window.vue_component_lib.GridVueWrapper = function() {
        return jQueryLoadHtml("./grid-wrapper/grid-vue-wrapper.html")
            .then(function (resp) {
                var model = getModel(resp);
                return model;
            }).catch(function (err) {
                console.log(err.message);
            });
    }
     
})();


