function jQueryLoadHtml(url) {
    return new Promise(function (res, rej) {
        return $.ajax({
            url: url,
            dataType: 'html',
            async: false
        }).then(function (resp) { res(resp) })
            .catch(function (err) { rej(err) });
    });
}

function uuidV4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};
