Vue Nested-List

Overview:

Infinitely nested-list with independent (from sibling lists) sorting on multiple columns as well as independent pagination

Tech:

  * Vue 3.0
  * Bootstrap 4.0
  * Lodash
  * Bluebird
  * jQuery
  * Pure HTML, javascript, CSS; no scaffolding, webpack, transpiler, bundling etc.
    
Link:
<a href="https://broadwaybytes.gitlab.io/vue-nested-list">demo</a>
